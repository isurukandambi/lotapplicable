﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LotApplicable.ORM;

namespace LotApplicable.AppCode
{
    public partial class FirstPage : System.Web.UI.Page
    {
        DatabaseEntites db;

        int productId;
        int Qty = 0;
        int alocated = 0;
        int undilev = 0;
        int reqstQty = 0;
        int remainQty = 0;
        int stackQty = 0;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        ////For Mannual Allocation
        private void manualAlocationLot()
        {
            db = new DatabaseEntites();

            int freeBalenceQty = 0;

            var checkitems = (from c in db.Test_LocInfo // Test_LocInfo = INV_LOCINFO
                              where c.INV_MARSTER_ID == productId && c.ON_HAND_QTY > 0
                              select new
                              {
                                  c.ON_HAND_QTY,
                                  c.QTY_ALLOCATED,
                                  c.QTY_UNDELIVERED
                              });

            if (checkitems.Count() > 0)
            {
                foreach (var itm in checkitems)
                {
                    freeBalenceQty = freeBalenceQty + Convert.ToInt32(itm.ON_HAND_QTY) - Convert.ToInt32(itm.QTY_ALLOCATED) - Convert.ToInt32(itm.QTY_UNDELIVERED);
                }
            }
            //Response.Write(freeBalenceQty);
            if (freeBalenceQty >= reqstQty)
            {
                remainQty = reqstQty;
                
                while (reqstQty > stackQty)
                {
                    var items = (from i in db.Test_LocInfo // Test_LocInfo = INV_LOCINFO
                                 where i.INV_MARSTER_ID == productId && i.ON_HAND_QTY - i.QTY_ALLOCATED - i.QTY_UNDELIVERED > 0
                                 select new
                                 {
                                     i.ID,
                                     i.BATCH_NO,
                                     i.ON_HAND_QTY,
                                     i.QTY_ALLOCATED,
                                     i.QTY_UNDELIVERED,
                                     i.INV_MARSTER_ID
                                 });
                    if (items.Count() > 0)
                    {
                        string max = items.Max(r => r.BATCH_NO).ToString();

                        var prod = (from p in db.Test_LocInfo // Test_LocInfo = INV_LOCINFO
                                    where p.INV_MARSTER_ID == productId && p.BATCH_NO == max
                                    select new
                                    {
                                        p.ID,
                                        p.BATCH_NO,
                                        p.ON_HAND_QTY,
                                        p.QTY_ALLOCATED,
                                        p.QTY_UNDELIVERED,
                                        p.INV_MARSTER_ID
                                    });

                        if (prod.Count() == 1)
                        {
                            Qty = Convert.ToInt32(prod.SingleOrDefault().ON_HAND_QTY);
                            alocated = Convert.ToInt32(prod.SingleOrDefault().QTY_ALLOCATED);
                            int freeBal = Qty - alocated - Convert.ToInt32(prod.SingleOrDefault().QTY_UNDELIVERED);

                            if (freeBal > 0)
                            {
                                if (remainQty <= freeBal)
                                {
                                    int quantity = alocated + remainQty;
                                    stackQty = stackQty + freeBal;
                                    int id = prod.SingleOrDefault().ID;

                                    try
                                    {
                                        var dbCstInfo = db.Test_LocInfo.Where(w => w.ID == id);

                                        if (dbCstInfo != null)
                                        {
                                            foreach (var record in dbCstInfo)
                                            {
                                                record.QTY_ALLOCATED = quantity;
                                            }
                                            db.SaveChanges();
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        Response.Write(e.StackTrace + "   " + e.Message);
                                    }

                                    Response.Write("Complete ::: Batch No - " + prod.FirstOrDefault().BATCH_NO + "Quantity - " + quantity);
                                }
                                else
                                {
                                    int quantity = alocated + freeBal;
                                    stackQty = stackQty + freeBal;
                                    remainQty = remainQty - freeBal;
                                    int id = prod.SingleOrDefault().ID;

                                    try
                                    {
                                        var dbCstInfo = db.Test_LocInfo.Where(w => w.ID == id);

                                        if (dbCstInfo != null)
                                        {
                                            foreach (var record in dbCstInfo)
                                            {
                                                record.QTY_ALLOCATED = quantity;
                                            }
                                            db.SaveChanges();
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        Response.Write(e.StackTrace + "   " + e.Message);
                                    }

                                    Response.Write("InComplete :: Batch No - " + prod.FirstOrDefault().BATCH_NO + "Quantity - " + quantity);
                                }
                            }
                            //else
                            //{
                            //    Response.Write("No Free Balence Quantity.");
                            //}
                        }
                    }
                    else
                    {
                        Response.Write("No Free Balence Quantity.");
                        break;
                    }
                }
            }
            else
            {
                Response.Write("Can't issue item, because On hand Quantity not enough for request items.");
            }

            //Response.Write(freeBalenceQty.ToString());
        }

        //For Print Invoice
        private void invoiceApplicable()
        {
            db = new DatabaseEntites();

            int freeBalenceQty = 0;

            var checkitems = (from c in db.Test_LocInfo // Test_LocInfo = INV_LOCINFO
                              where c.INV_MARSTER_ID == productId && c.ON_HAND_QTY > 0
                              select new
                              {
                                  c.ON_HAND_QTY,
                                  c.QTY_ALLOCATED,
                                  c.QTY_UNDELIVERED
                              });

            if (checkitems.Count() > 0)
            {
                foreach (var itm in checkitems)
                {
                    freeBalenceQty = freeBalenceQty + Convert.ToInt32(itm.ON_HAND_QTY) - Convert.ToInt32(itm.QTY_ALLOCATED) - Convert.ToInt32(itm.QTY_UNDELIVERED);
                }
            }

            if (freeBalenceQty >= reqstQty)
            {
                remainQty = reqstQty;

                while (reqstQty > stackQty)
                {
                    var items = (from i in db.Test_LocInfo // Test_LocInfo = INV_LOCINFO
                                 where i.INV_MARSTER_ID == productId && i.ON_HAND_QTY - i.QTY_ALLOCATED - i.QTY_UNDELIVERED > 0
                                 select new
                                 {
                                     i.ID,
                                     i.BATCH_NO,
                                     i.ON_HAND_QTY,
                                     i.QTY_ALLOCATED,
                                     i.QTY_UNDELIVERED,
                                     i.INV_MARSTER_ID
                                 });
                    if (items.Count() > 0)
                    {
                        string min = items.Min(r => r.BATCH_NO).ToString();

                        var prod = (from p in db.Test_LocInfo // Test_LocInfo = INV_LOCINFO
                                    where p.INV_MARSTER_ID == productId && p.BATCH_NO == min
                                    select new
                                    {
                                        p.ID,
                                        p.BATCH_NO,
                                        p.ON_HAND_QTY,
                                        p.QTY_ALLOCATED,
                                        p.QTY_UNDELIVERED,
                                        p.INV_MARSTER_ID
                                    });

                        if (prod.Count() == 1)
                        {
                            Qty = Convert.ToInt32(prod.SingleOrDefault().ON_HAND_QTY);
                            alocated = Convert.ToInt32(prod.SingleOrDefault().QTY_ALLOCATED);
                            undilev = Convert.ToInt32(prod.SingleOrDefault().QTY_UNDELIVERED);
                            int freeBal = Qty - alocated - undilev;

                            if (freeBal > 0)
                            {
                                if (remainQty <= freeBal)
                                {
                                    int quantity = undilev + remainQty;
                                    stackQty = stackQty + freeBal;
                                    int id = prod.SingleOrDefault().ID;

                                    try
                                    {
                                        var dbCstInfo = db.Test_LocInfo.Where(w => w.ID == id);

                                        if (dbCstInfo != null)
                                        {
                                            foreach (var record in dbCstInfo)
                                            {
                                                record.QTY_UNDELIVERED = quantity;
                                            }
                                            db.SaveChanges();
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        Response.Write(e.StackTrace + "   " + e.Message);
                                    }

                                    Response.Write("Complete ::: Batch No - " + prod.FirstOrDefault().BATCH_NO + "Quantity - " + quantity);
                                }
                                else
                                {
                                    int quantity = undilev + freeBal;
                                    stackQty = stackQty + freeBal;
                                    remainQty = remainQty - freeBal;
                                    int id = prod.SingleOrDefault().ID;

                                    try
                                    {
                                        var dbCstInfo = db.Test_LocInfo.Where(w => w.ID == id);

                                        if (dbCstInfo != null)
                                        {
                                            foreach (var record in dbCstInfo)
                                            {
                                                record.QTY_UNDELIVERED = quantity;
                                            }
                                            db.SaveChanges();
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        Response.Write(e.StackTrace + "   " + e.Message);
                                    }

                                    Response.Write("InComplete :: Batch No - " + prod.FirstOrDefault().BATCH_NO + "Quantity - " + quantity);
                                }
                            }
                            //else
                            //{
                            //    Response.Write("No Free Balence Quantity.");
                            //}
                        }
                    }
                    else
                    {
                        Response.Write("No Free Balence Quantity.");
                        break;
                    }                                     
                }
            }
            else
            {
                Response.Write("Can't issue item, because On hand Quantity not enough for request items.");
            }
        }

        //For Sales Issues
        private void salesIssue()
        {
            db = new DatabaseEntites();

            int freeBalenceUndelQty = 0;

            var checkitems = (from c in db.Test_LocInfo // Test_LocInfo = INV_LOCINFO
                              where c.INV_MARSTER_ID == productId && c.QTY_UNDELIVERED > 0
                              select new
                              {
                                  c.ON_HAND_QTY,
                                  c.QTY_ALLOCATED,
                                  c.QTY_UNDELIVERED
                              });

            if (checkitems.Count() > 0)
            {
                foreach (var itm in checkitems)
                {
                    freeBalenceUndelQty = freeBalenceUndelQty + Convert.ToInt32(itm.QTY_UNDELIVERED);
                }
            }

            if (freeBalenceUndelQty >= reqstQty)
            {
                remainQty = reqstQty;

                while (reqstQty > stackQty)
                {
                    var items = (from i in db.Test_LocInfo // Test_LocInfo = INV_LOCINFO
                                 where i.INV_MARSTER_ID == productId && i.QTY_UNDELIVERED > 0
                                 select new
                                 {
                                     i.ID,
                                     i.BATCH_NO,
                                     i.ON_HAND_QTY,
                                     i.QTY_ALLOCATED,
                                     i.QTY_UNDELIVERED,
                                     i.INV_MARSTER_ID
                                 });
                    if (items.Count() > 0)
                    {
                        string min = items.Min(r => r.BATCH_NO).ToString();

                        var prod = (from p in db.Test_LocInfo // Test_LocInfo = INV_LOCINFO
                                    where p.INV_MARSTER_ID == productId && p.BATCH_NO == min
                                    select new
                                    {
                                        p.ID,
                                        p.BATCH_NO,
                                        p.ON_HAND_QTY,
                                        p.QTY_ALLOCATED,
                                        p.QTY_UNDELIVERED,
                                        p.INV_MARSTER_ID
                                    });

                        if (prod.Count() == 1)
                        {
                            Qty = Convert.ToInt32(prod.SingleOrDefault().ON_HAND_QTY);
                            alocated = Convert.ToInt32(prod.SingleOrDefault().QTY_ALLOCATED);
                            undilev = Convert.ToInt32(prod.SingleOrDefault().QTY_UNDELIVERED);
                            //int freeBal = Qty - alocated - Convert.ToInt32(prod.SingleOrDefault().QTY_UNDELIVERED);

                            if (undilev > 0 || Qty >= undilev)
                            {
                                if (remainQty <= undilev)
                                {
                                    int quantity = Qty - remainQty;
                                    int unDelquantity = undilev - remainQty;
                                    stackQty = stackQty + undilev;
                                    int id = prod.SingleOrDefault().ID;

                                    try
                                    {
                                        var dbCstInfo = db.Test_LocInfo.Where(w => w.ID == id);

                                        if (dbCstInfo != null)
                                        {
                                            foreach (var record in dbCstInfo)
                                            {
                                                record.ON_HAND_QTY = quantity;
                                                record.QTY_UNDELIVERED = unDelquantity;
                                            }
                                            db.SaveChanges();
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        Response.Write(e.StackTrace + "   " + e.Message);
                                    }

                                    Response.Write("Complete ::: Batch No - " + prod.FirstOrDefault().BATCH_NO + "Quantity - " + quantity);
                                }
                                else
                                {
                                    int quantity = Qty - undilev;
                                    int unDelquantity = 0;   
                                    stackQty = stackQty + undilev;
                                    remainQty = remainQty - undilev;
                                    int id = prod.SingleOrDefault().ID;

                                    try
                                    {
                                        var dbCstInfo = db.Test_LocInfo.Where(w => w.ID == id);

                                        if (dbCstInfo != null)
                                        {
                                            foreach (var record in dbCstInfo)
                                            {
                                                record.ON_HAND_QTY = quantity;
                                                record.QTY_UNDELIVERED = unDelquantity;
                                            }
                                            db.SaveChanges();
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        Response.Write(e.StackTrace + "   " + e.Message);
                                    }

                                    Response.Write("InComplete :: Batch No - " + prod.FirstOrDefault().BATCH_NO + "Quantity - " + quantity);
                                }
                            }
                            else
                            {
                                Response.Write("Unable to Issue because Undelivered Quantity is null or On Hand Quantity is not enough for deliver.");
                            }
                        }
                    }
                    else
                    {
                        Response.Write("No Un Delivered Quantity.");
                        break;
                    }
                }
            }
            else
            {
                Response.Write("Can't issue item, because Undelivery Quantity not enough for request items.");
            }
        }

        private void lotApplicable()
        {
            db = new DatabaseEntites();

            int onHandQty = 0;

            var checkitems = (from i in db.Test_LocInfo // Test_LocInfo = INV_LOCINFO
                         where i.INV_MARSTER_ID == productId && i.ON_HAND_QTY > 0
                         select new
                         {
                             i.ON_HAND_QTY
                         });

            if (checkitems.Count() > 0)
            {
                foreach (var itm in checkitems)
                {
                    onHandQty = onHandQty + Convert.ToInt32(itm.ON_HAND_QTY);
                }
            }

            if (onHandQty >= reqstQty)
            {
                remainQty = reqstQty;

                while (reqstQty > stackQty)
                {
                    var items = (from i in db.Test_LocInfo // Test_LocInfo = INV_LOCINFO
                                 where i.INV_MARSTER_ID == productId && i.ON_HAND_QTY > 0
                                 select new
                                 {
                                     i.ID,
                                     i.BATCH_NO,
                                     i.ON_HAND_QTY,
                                     i.INV_MARSTER_ID
                                 });

                    if (items.Count() > 0)
                    {
                        string min = items.Min(r => r.BATCH_NO).ToString();

                        var prod = (from p in db.Test_LocInfo // Test_LocInfo = INV_LOCINFO
                                    where p.INV_MARSTER_ID == productId && p.BATCH_NO == min
                                    select new
                                    {
                                        p.ID,
                                        p.BATCH_NO,
                                        p.ON_HAND_QTY,
                                        p.INV_MARSTER_ID
                                    });

                        if (prod.Count() == 1)
                        {
                            Qty = Convert.ToInt32(prod.SingleOrDefault().ON_HAND_QTY);

                            if (remainQty <= Qty)
                            {
                                int quantity = Qty - remainQty;
                                stackQty = stackQty + Qty;
                                int id = prod.SingleOrDefault().ID;

                                try
                                {
                                    var dbCstInfo = db.Test_LocInfo.Where(w => w.ID == id);

                                    if (dbCstInfo != null)
                                    {
                                        foreach (var record in dbCstInfo)
                                        {
                                            record.ON_HAND_QTY = quantity;
                                        }
                                        db.SaveChanges();
                                    }
                                }
                                catch (Exception e)
                                {
                                    Response.Write(e.StackTrace + "   " + e.Message);
                                }

                                Response.Write("Complete ::: Batch No - " + prod.FirstOrDefault().BATCH_NO + "Quantity - " + quantity);
                            }
                            else
                            {
                                int quantity = reqstQty - remainQty;
                                stackQty = stackQty + Qty;
                                remainQty = remainQty - Qty;
                                int id = prod.SingleOrDefault().ID;

                                try
                                {
                                    var dbCstInfo = db.Test_LocInfo.Where(w => w.ID == id);

                                    if (dbCstInfo != null)
                                    {
                                        foreach (var record in dbCstInfo)
                                        {
                                            record.ON_HAND_QTY = 0;
                                        }
                                        db.SaveChanges();
                                    }
                                }
                                catch (Exception e)
                                {
                                    Response.Write(e.StackTrace + "   " + e.Message);
                                }

                                Response.Write("InComplete :: Batch No - " + prod.FirstOrDefault().BATCH_NO + "Quantity - " + quantity);
                            }

                        }
                        else
                        {
                            Response.Write("Select Item Batch is Unavailable");
                            break;
                        }
                    }
                }
            }
            else
            {
                Response.Write("Can't issue item, because On hand Quantity not enough for request items.");
            }
        }

        protected void btnIssue_Click(object sender, EventArgs e)
        {
            productId = Convert.ToInt32(txtId.Text);
            reqstQty = Convert.ToInt32(txtQty.Text);

            //lotApplicable();
            //manualAlocationLot();
            //invoiceApplicable();
            salesIssue();
        }
    }
}